# CTFd Personal Challenge plugin

This plugin enhances CTFd by individual flags and personalized challenges. The personalized challenges enable setting an individual flag for each player.

## Prerequisites

CTFd version 3.2.1.

## Installation

### Linux (Ubuntu/Debian)

0. Download CTFd 3.2.1: `$ git clone -b '3.2.1' --single-branch https://github.com/CTFd/CTFd.git`
1. Navigate into the CTFd directory: `$ cd <path>/CTFd`
2. Clone this project with `$ git clone https://gitlab.fi.muni.cz/cybersec/apg/ctfd-personal_challenge-plugin.git`
3. Copy the downloaded plugin into CTFd directory: `$ cp -R -L -f ctfd-personal_challenge-plugin/. CTFd/plugins/ && rm -rf ctfd-personal_challenge-plugin`

## Windows

0. Download CTFd 3.2.1: `$ git clone -b '3.2.1' --single-branch https://github.com/CTFd/CTFd.git --config core.autocrlf=input`
1. Follow steps 1-3 for Linux.

## Update

0. Navigate into the CTFd directory `$ cd <path>/CTFd`
1. Navigate into the plugins directory `$ cd CTFd/plugins`
2. Pull new version of the project with `$ git pull`

## Usage

After the installation, run `$ docker-compose up` in the CTFd directory or `$ docker-compose -f /<absolute_path>/CTFd/docker-compose.yml up`.

Now you have a clean CTFd installation with Personal Challenge plugin. Once you setup the CTFd instance, you can create _personal_ challenges. See [the plugin README.md](personal_challenges/README.md) for more details.
